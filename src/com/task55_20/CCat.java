package com.task55_20;

public class CCat extends CPet implements IRunable {
	@Override
	public void run() {
		System.out.println("Cat running");

	} 
	@Override
	protected void playing() {
		System.out.println("Cat playing");

	}

	@Override
	public void animalSound() {
		System.out.println("Cat sound meo meo");

	}

	@Override
	public void eat() {
		System.out.println("Cat eat");

	}
	@Override
	public String toString() {
		return "MyCat: [age=" + age + ", name=" + name + "]";
	}
}
