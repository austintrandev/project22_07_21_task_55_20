package com.task55_20;

import java.util.ArrayList;


public class CPerson {
	/**
	 * @param id
	 * @param age
	 * @param firstName
	 * @param lastName
	 * @param cars
	 */
	public CPerson(int id, int age, String firstName, String lastName, ArrayList<String> cars) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.cars = cars;
	}

	/**
	 * @param id
	 * @param age
	 * @param firstName
	 * @param lastName
	 */
	public CPerson(int id, int age, String firstName, String lastName) {
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public CPerson() {
	}

	private int id = 0;
	private int age = 0;
	private String firstName = "";
	private String lastName = "";
	private ArrayList<String> cars = new ArrayList<String>();
	public String country = "Viet Nam";
	private ArrayList<CPet> pets = new ArrayList<CPet>();

	/**
	 * @return the pets
	 */
	public ArrayList<CPet> getPets() {
		return pets;
	}

	/**
	 * @param pets the pets to set
	 */
	public void setPets(ArrayList<CPet> pets) {
		this.pets = pets;
	}

	public void addPet(CPet newPet) {
		this.pets.add(newPet);
	}

	public void removePet(int petIndex) {
		this.pets.remove(petIndex);
	}

	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the cars
	 */
	public ArrayList<String> getCars() {
		return cars;
	}

	/**
	 * th�m danh s�ch oto v�o user n�y
	 * 
	 * @param cars the cars to set
	 */
	public void addCars(ArrayList<String> cars) {
		this.cars.addAll(cars);
	}

	/**
	 * th�m 1 cars v�o danh s�ch user n�y.
	 * 
	 * @param car the car to set
	 */
	public void addCar(String car) {
		this.cars.add(car);
	}

	/**
	 * @param cars the cars to set
	 */
	public void setCars(ArrayList<String> cars) {
		this.cars = cars;
	}

	public void printPerson() {
		System.out.println(this.getFullName() + " " + this.getAge() + " " + this.id);
		System.out.println(this.country);
		System.out.println(this.cars);
		System.out.println("PET list: "+this.pets);

	}
}
