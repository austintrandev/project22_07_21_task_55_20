package com.task55_20;

public abstract class CPet extends CAnimal {
	public CPet(int age, String name) {
		super();
		this.age = age;
		this.name = name;
	}

	public CPet() {
		super();
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public int age;
	public String name;

	protected void bark() {
		System.out.println("barking...");
	}

	protected void print() {
		System.out.println("Pet name: " + name);
		System.out.println("Pet age: " + this.age);
	}

	protected void playing() {
		System.out.println("playing...");
	}

	public AnimalClass getAnimalClass() {
		return AnimalClass.mammals;
	}
}
