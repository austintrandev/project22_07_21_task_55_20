package com.task55_20;


public class MainPet {

	public static void main(String[] args) {
		CPet pet = new CCat();
		System.out.println(" pet.getAnimalClass(): " + pet.getAnimalClass());
		((CPet)pet).print();
		System.out.println("---------------------");
		CAnimal animal1 = new CDog();
		((CDog) animal1).bark();
		animal1.animalSound();
		animal1.eat();
		System.out.println("---------------------");
		animal1 = new CCat();
		animal1.animalSound();
		animal1.eat();
		
		
	}

}
