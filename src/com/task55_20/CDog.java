package com.task55_20;

public class CDog extends CPet implements IBarkable,IRunable {
	@Override
	public void bark() {
		System.out.println("Gau gau");

	}

	@Override
	protected void playing() {
		System.out.println("Dog playing");

	}

	@Override
	public void animalSound() {
		System.out.println("Dog sound");

	}

	@Override
	public void eat() {
		System.out.println("Dog eat");

	}

	@Override
	public void run() {
		System.out.println("Dog running");
		
	}
	@Override
	public void print() {
		System.out.println("Dog name: " + name);
		System.out.println("Dog age: " + this.age);
		
	}
	
	@Override
	public String toString() {
		return "MyDog: [age=" + age + ", name=" + name + "]";
	}
}
